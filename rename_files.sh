#!/bin/bash

EXIT_SUCCESS=0
EXIT_FAILURE=1

if [ $# -lt 1 ]
then
    echo "Uso: $0 <dir>"
    exit $EXIT_FAILURE
fi

DIR=$1
cd $DIR

# BOZZA - non funzionante
function recursdir() {

    for i in *
    do
        ls $i

        if [ -d "$i" ]
        then
            cd $i
            recursdir $i
        fi
    done

    cd ..
}

recursdir

exit $EXIT_SUCCESS
