#!/bin/bash
# 
# Scope: send file to remote host via scp
# 
# This is useful for me when I want to copy single config file or tar gzipped 
# file across the network.

EXIT_SUCCESS=0
EXIT_FAILURE=1

# I have the same user login on different machines
REMOTE_USER=`whoami`        # remote user
REMOTE_HOST="10.0.0.6"      # remote host
REMOTE_DIR="/tmp"           # remote dir (default is ok)
REMOTE_PORT=22              # remote port (default is ok)

if [ "$#" -lt 1 ]
then
    echo "Use: ${0} <file to send on remote host>"
    exit $EXIT_FAILURE
fi

scp -P ${PORT} ${1} ${REMOTE_USER}@${REMOTE_HOST}:${REMOTE_DIR}/${1}_cp

# copy multiple files accross the network; 
# scp uses secure channel, rsync does not, but you can force rsync to use ssh: 
# rsync -azvh -e ssh $LOCAL_DIR $REMOTE_USER@$REMOTE_HOST:$REMOTE_DIR

exit $EXIT_SUCCESS
